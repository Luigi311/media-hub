/*
 * Copyright © 2021-2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <QLoggingCategory>

namespace lomiri {
namespace MediaHubService {

// Log returns the lomiri::MediaHubService-wide configured logger instance.
// Save to call before/after main.
const QLoggingCategory &Log();

} // namespace MediaHubService
} // namespace lomiri

#define MH_TRACE(...) qCDebug(::lomiri::MediaHubService::Log(), __VA_ARGS__)
#define MH_DEBUG(...) qCDebug(::lomiri::MediaHubService::Log(), __VA_ARGS__)
#define MH_INFO(...) qCInfo(::lomiri::MediaHubService::Log(), __VA_ARGS__)
#define MH_WARNING(...) qCWarning(::lomiri::MediaHubService::Log(), __VA_ARGS__)
#define MH_ERROR(...) qCCritical(::lomiri::MediaHubService::Log(), __VA_ARGS__)
#define MH_FATAL(...) qCCritical(::lomiri::MediaHubService::Log(), __VA_ARGS__)

#endif
